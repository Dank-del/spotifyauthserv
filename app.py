#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file includes small webserver to handle
& save incoming callback data from spotify API
"""
import hashlib
import os
from abc import ABC

import requests
import tornado.web

bot_username = os.environ.get("BOT_USERNAME", "")
PORT = os.environ.get("PORT", 8080)
JBK = os.environ.get("JBK", "")
ADDRESS = os.environ.get("ADDRESS", "0.0.0.0")


class MainHandler(tornado.web.RequestHandler, ABC):
    def get(self):
        self.write("Root")


class SpotifyCallback(tornado.web.RequestHandler, ABC):
    """
    This class handles GET requests done by spotify
    upon Oauth2 and saves them in database.

    """

    def get(self):
        if self.get_argument("code", ""):
            code = self.get_argument("code", "")
            url = 'https://jsonblob.com/api/{}'.format(JBK)
            key = hashlib.sha1(bytearray(code.encode('utf-8'))).hexdigest()[:4]
            jsn = requests.get(url).json()
            jsn.update({key: code})
            requests.put(url, json=jsn)

            print("user logged in successfully")
            self.redirect("https://t.me/{}?start={}".format(bot_username, key))


urls = [(r"/", MainHandler), (r"/callback", SpotifyCallback)]


def start() -> None:
    """
    Function to start the app
    """

    app = tornado.web.Application(urls)
    app.listen(PORT, address=ADDRESS)

    print("!===== Tornado server started =====!")
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    start()
